<?php

use Faker\Generator as Faker;

$factory->define(App\Contato::class, function (Faker $faker) {

    return [
            'nome' => $faker->name,
            'telefone' => $faker->unique()->phoneNumber,
            'email' => $faker->unique()->email,
            'data_nascimento' => '01-06-1995',
            'cep' => $faker->postcode,
            'logradouro' => $faker->streetName,
            'numero' => $faker->buildingNumber,
            'bairro' => '-',
            'localidade' => $faker->city,
            'uf' => $faker->stateAbbr,
            'nota' => 'Factory, Faker and Seeder'
    ];
});
