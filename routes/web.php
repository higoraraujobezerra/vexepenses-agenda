<?php

use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'ContatoController@index');

Route::group(['middleware'=>'auth', 'prefix'=>'contatos'], function() {
    Route::get('/', 'ContatoController@index');
    Route::get('/add', 'ContatoController@create');
    Route::post('/', 'ContatoController@store');
    Route::get('{id}', 'ContatoController@show');
    Route::get('/edit/{id}', 'ContatoController@edit');
    Route::put('{id}', 'ContatoController@update');
    Route::delete('{id}', 'ContatoController@destroy');
});

Route::get('/envio-email', function(){
    
    $user = new User;
    $user->name = Auth::user()->name;
    $user->Email = Auth::user()->email;
    
    //return new \App\Mail\mailAgenda($user);
    \App\Jobs\mailAgenda::dispatch($user->delay(now()->addSeconds('15')));
});