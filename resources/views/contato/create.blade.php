@extends('layouts.app')

@section("js")
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Contato</div>
                <form action="{{ url('contatos') }}" method="post" enctype="multipart/form-data">
                    <div class="card-body">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="nome"><b>Nome completo</b></label>
                            <input type="text" required class="form-control{{$errors->has('nome') ? ' is-invalid':''}}" value="{{ old('nome') }}" id="nome" name="nome">
                            <div class="invalid-feedback">{{ $errors->first('nome') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="telefone"><b>Telefone</b></label>
                            <input type="text" required class="form-control{{$errors->has('telefone') ? ' is-invalid':''}}" value="{{ old('telefone') }}" id="telefone" name="telefone">
                            <div class="invalid-feedback">{{ $errors->first('telefone') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="email"><b>E-mail</b></label>
                            <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid':''}}" value="{{ old('email') }}" id="email" name="email" placeholder="email@provedor.com.br">
                            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento"><b>Data de nascimento</b></label>
                            <input type="text" class="form-control{{$errors->has('data_nascimento') ? ' is-invalid':''}}" id="data_nascimento" value="{{ old('data_nascimento') }}" name="data_nascimento" placeholder="00/00/0000">
                            <div class="invalid-feedback">{{ $errors->first('data_nascimento') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="cep"><b>CEP</b></label>
                            <input type="text" class="form-control{{$errors->has('cep') ? ' is-invalid':''}}" id="cep" value="{{ old('cep') }}" name="cep" placeholder="00000000">
                            <div class="invalid-feedback">{{ $errors->first('cep') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="logradouro"><b>Logradouro</b></label>
                            <input type="text" class="form-control{{$errors->has('logradouro') ? ' is-invalid':''}}" id="logradouro" value="{{ old('logradouro') }}" name="logradouro">
                            <div class="invalid-feedback">{{ $errors->first('logradouro') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="numero"><b>Número</b></label>
                            <input type="text" class="form-control{{$errors->has('numero') ? ' is-invalid':''}}" id="numero" value="{{ old('numero') }}" name="numero">
                            <div class="invalid-feedback">{{ $errors->first('numero') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="complemento"><b>Complemento</b></label>
                            <input type="text" class="form-control{{$errors->has('complemento') ? ' is-invalid':''}}" id="complemento" value="{{ old('complemento') }}" name="complemento">
                            <div class="invalid-feedback">{{ $errors->first('complemento') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="bairro"><b>Bairro</b></label>
                            <input type="text" class="form-control{{$errors->has('bairro') ? ' is-invalid':''}}" id="bairro" value="{{ old('bairro') }}" name="bairro">
                            <div class="invalid-feedback">{{ $errors->first('bairro') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="cidade"><b>Cidade</b></label>
                            <input type="text" class="form-control{{$errors->has('localidade') ? ' is-invalid':''}}" id="localidade" value="{{ old('localidade') }}" name="localidade">
                            <div class="invalid-feedback">{{ $errors->first('localidade') }}</div>
                        </div>

                        <div class="form-group">
                            <label for="uf"><b>UF</b></label>
                            <input type="text" class="form-control{{$errors->has('uf') ? ' is-invalid':''}}" id="uf" value="{{ old('uf') }}" name="uf">
                            <div class="invalid-feedback">{{ $errors->first('uf') }}</div>
                        </div>
                        
                        <div class="form-group">
                            <label for="avatar"><b>Avatar</b></label>
                            <input type="file" class="form-control-file{{$errors->has('avatar') ? ' is-invalid':''}}" id="avatar" name="avatar" accept=".jpg, .jpeg, .png .gif">
                            <div class="invalid-feedback" style="display:inherit">{{ $errors->first('avatar') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="nota"><b>Nota</b></label>
                            <textarea class="form-control" id="nota" name="nota" rows="5">{{ old('nota') }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <a href="#" onclick="history.back()" class="btn btn-secondary">Voltar</a>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section("script")
    <script type="text/javascript" > 
    $(document).ready(function() {
        $("#cep").focusout(function(){
            $.ajax({
                url: 'https://viacep.com.br/ws/'+$(this).val()+'/json/unicode/',
                dataType: 'json',
                success: function(resposta){
                    
                    $("#logradouro").val(resposta.logradouro);
                    $("#bairro").val(resposta.bairro);
                    $("#localidade").val(resposta.localidade);
                    $("#uf").val(resposta.uf);
                    
                    $("#numero").focus();
                }
            });
        });
    });
</script>
@endsection