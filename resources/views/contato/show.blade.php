@extends('layouts.app')

@section('stylecss')
<style>
.form-control-static {
    font-weight: bold;
}
.img-avatar {
    max-width: 100%;
    border: 1px solid #c0c0c0;
    border-radius: 5px;
    margin-bottom: 15px;
}
</style>
@endsection

@section('script')
<script type="text/javascript">
function validate_delete() {
    return confirm('Excluir o registro atual? Essa ação não pode ser desfeita.');
}
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><U>CONTATO</u></div>
                <form action="{{ url('contatos/'.$data->id) }}" method="post" onsubmit="return validate_delete()">
                    <div class="card-body">
                        @method('DELETE')

                        {{ csrf_field() }}

                        <div class="row">
                                <div class="col-sm-4">
                                    <img src="{{ $data->avatar_image }}" class="responsive">
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nome"><u><b>Nome:</b></u></label>
                                        <p class="form-control-static">{{ $data->nome }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="telefone"><u><b>Telefone:</b></u></label>
                                        <p class="form-control-static">{{ $data->telefone }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><u><b>E-mail:</b></u></label>
                                        <p class="form-control-static">{{ $data->email }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="data_nascimento"><u><b>Data de Nascimento:</b></u></label>
                                        <p class="form-control-static">{{ $data->data_nascimento }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="cep"><u><b>CEP:</b></u></label>
                                        <p class="form-control-static">{{ $data->cep }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="logradouro"><u><b>Logradouro:</b></u></label>
                                        <p class="form-control-static">{{ $data->logradouro }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="numero"><u><b>Número:</b></u></label>
                                        <p class="form-control-static">{{ $data->numero }}</p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="complemento"><u><b>Complemento:</b></u></label>
                                        <p class="form-control-static">{{ $data->complemento }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="bairro"><u><b>Bairro:</b></u></label>
                                        <p class="form-control-static">{{ $data->bairro }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="localidade"><u><b>Cidade:</b></u></label>
                                        <p class="form-control-static">{{ $data->localidade }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="uf"><u><b>UF:</b></u></label>
                                        <p class="form-control-static">{{ $data->uf }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="nota"><u><b>Nota:</b></u></label>
                                        <p class="form-control-static">{{ $data->nota }}</p>
                                    </div>
                                </div>
                                

                            </div><!-- /.row -->
                        </div>
                    <div class="card-footer text-right">
                        <a href="#" onclick="history.back()" class="btn btn-secondary">Voltar</a>
                        <button type="submit" class="btn btn-danger">Excluir</button>
                        <a href="{{ url('contatos/edit/'.$data->id) }}" class="btn btn-primary">Editar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
