@extends('layouts.app')

@section('stylecss')
<style media="screen">
    .img-avatar-xs {
        width: 30px;
        height: 30px;
        border: 1px solid #c0c0c0;
        border-radius: 5px;
    }
    .a-line {
        line-height: 30px;
    }
</style>

@endsection

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
             AGENDA 
            <a href="{{ url('contatos/add') }}" class="btn btn-primary btn-sm float-right">Adicionar contato</a>
        </div>
        <div class="card-body p-0">
            <div class="table-responsive border-0">
                <table class="table table-striped" style="margin-bottom: inherit">
                    <tbody>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nome</th>
                            <th class="text-center">Telefone</th>
                            <th class="text-center">E-mail</th>
                        </tr>
                        @foreach ($contatos as $contato)
                        <tr>
                            <td class="text-center">
                                <a  href="{{ url('contatos/'.$contato->id) }}">
                                    <img src="{{ $contato->avatar_image }}" class="img-avatar-xs">
                                </a>
                            </td>
                            <td class="text-center" class="d-none d-md-table-cell"><a class='a-line' href="{{ url('contatos/'.$contato->id) }}">{{ $contato->nome }}</a></td>
                            <td class="text-center" class="d-none d-md-table-cell"><a class='a-line'  href="{{ url('contatos/'.$contato->id) }}">{{ $contato->telefone }}</a></td>
                            <td class="text-center" class="d-none d-md-table-cell"><a class='a-line' href="{{ url('contatos/'.$contato->id) }}">{{ $contato->email }}</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
