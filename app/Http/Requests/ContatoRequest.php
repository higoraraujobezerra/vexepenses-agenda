<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome'              => 'required|min:2|max:100',
            'telefone'          => 'required|max:15',
            'email'             => 'nullable|sometimes|email|max:200|unique:contatos',
            'data_nascimento'   => 'nullable|date_format:"d/m/Y',
            'cep'               => 'nullable|min:8|max:8',
            'logradouro',
            'numero',
            'complemento',
            'bairro',
            'localidade',
            'uf',
            'avatar'            => 'nullable|sometimes|image|mimes:jpg,jpeg,png,gif'
        ];

        switch($this->method()){
            case "POST":
                return [
                    'nome'              => 'required|min:2|max:100',
                    'telefone'          => 'required|max:15',
                    'email'             => 'nullable|sometimes|email|max:200|unique:contatos',
                    'data_nascimento'   => 'nullable|date_format:"d/m/Y',
                    'cep'               => 'nullable|min:8|max:8',
                    'logradouro',
                    'numero',
                    'complemento',
                    'bairro',
                    'localidade',
                    'uf',
                    'avatar'            => 'nullable|sometimes|image|mimes:jpg,jpeg,png,gif'
                ];
                break;

            case "PUT":
                $rules["email"] = "|unique:contatos,email,".$this->id;
                $rules["telefone"] = "|unique:contatos,telefone,".$this->id;
                return $rules;
                break;
            default:break;
        }
        return $rules;
    }
    public function messages()
    {
        return [
            'nome.required' => 'O campo Nome é obrigatório.',
            'telefone.requires' => 'O campo Telefone é obrigatório.',
            'email.email' => 'Informe um e-mail válido.',
            'data-nascimento.date_format' => 'O campo data deve ser no formato DD/MM/YYYY.',
            'cep' => 'O CEP é constituído de 8 algarismos.',
        ];
    }
}
