# PROJETO DESAFIO VEXPENSES

Desevolvimento de uma agenda de contatos.

## Instalação
- Clonar o repositório;
- Criar banco de dados;
- Criar arquivi .env e configurar os dados do banco de dados criado;

## Terminal 
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan migrate:refresh -seed
- php artisan serve

## Acesso
- Criar novo registro;
